---
layout: page.njk
seoPageTitle: About Behind the Source and who made it
seoPageDescription: What is Behind the Source? It explores the web industry and those that build it. Sharing the varying ways people get into the web and their view on the industry
title: About

---

**Behind the Source** is a project by [Mike Street](https://www.mikestreety.co.uk/). It explores the people and products behind the web and what makes it. The project started as a series of interviews about the people, but has moved to a podcast about the tech.

## Podcast

The podcast talks to a guest each episode and explores the hows and whys of a particular tech or subject.

## Interviews

**[Read the interviews](/interviews/)**

The interviews are a series of blog posts created and curated. Each post is an interview with someone involved in creating the web and answers what they do and how they got there. Along with that, it explores their thoughts on the web industry and how it could be improved.

If you were to believe Twitter, you would think every developer is changing their tech stack every week. The latest framework is a must-use for building your website and if you're still using last week's one you are left behind. This isn't the case; this series of interviews aims to expose that.

Talking to real developers, each post highlights no-one is the same, there is no one "golden bullet" to get a job and that nearly everyone has technical debt in their toolkit. No matter how you view the tech industry, these posts highlight the many pitfalls and blessings within the web industry.

Interested in being interviewed? Read about the [interview process](/interviews/process/). Alternatively, [sign up](/signup/) to the newsletter to be kept up-to-date when a new interview goes live.

---

This site is run on [11ty](https://www.11ty.dev/), powered by [Cloudflare Pages](https://www.cloudflare.com/). If you are interesting in how it is made, you can find the repository on [Gitlab](https://gitlab.com/streety-sites/behind-the-source).
