---json
{
	"date": "2020-07-06",
	"title": "S01E05",
	"permalink": "/interviews/s01e05-al-power/",
	"heading": "One can’t help being inspired by going online and seeing what people are sharing",
	"intro": "<p>An absolutely legendary name, for a legendary hero. I was racking my brains as to how I met Al Power and I cannot, for the life of me, remember. We've known each other digitally for many years and have often bumped into each other at local conferences (including my own!). Al Power knows his stuff and (until I read this interview) I realised I didn't actually know that much about the man. He is one of the nicest people you'll meet and it is an honour to have him as part of this project.</p>",
	"name": "Al Power",
	"photo": "/assets/img/people/s01e05.jpg",
	"ogImage": "/assets/img/social-media/s01e05.png",
	"social": [
		{
			"title": "alpower",
			"type": "twitter"
		},
		{
			"href": "https://www.alpower.com",
			"title": "www.alpower.com",
			"type": "website"
		}
	],
	"body": [
		{
			"type": "question",
			"fields": {
				"question": "What is your job title (and feel free to talk about where you work)?",
				"answer": "<p>I am a <strong>Creative Director/Design lead</strong> for <a href=\"https://www.dabapps.com/\">DabApps</a>, a leading mobile/web application agency based in Brighton, UK. We help people design and develop great digital products.<br /></p>\n\n<p>I’ve done both managing others and also production work as part of my day-to-day. I don’t think there’s anything such as a ‘normal’ day as it varies week to week. </p>\n<p>I schedule in work and client meetings, and also help others on demand - i.e. if a developer has a question about something that wasn’t covered in designs I often help out fill in the gaps.</p>\n<p>I do find that setting out a portion of your day for uninterrupted work and the other half for meetings to be useful if you can. Once I learned that it was OK not always helping people immediately, but instead scheduling things at a certain time, it became much easier to reach and stay in flow. With the rise of asynchronous communication tools, things have become very interrupt-driven, and I’m trying to fight against this. </p>\n<p>Changing your Slack status to ‘head down - do not disturb’ then switching it off works wonders when you need to focus. People will always come and tap you on the shoulder if there is a real emergency.</p>\n<p>For me, as a designer, 50% of our job is design - the other 50% is communication - something that’s often lost on junior designers. </p>\n<p>Far too many designers have a hard time moving into management and feel that they aren’t doing real work, once they move over to more ‘people’ type things, when in fact giving guidance, critique and help to someone else who might be doing the work can be equally valid.</p>\n"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "I learned that it was OK not always helping people immediately"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "How did you get where you are today? What is your educational background? How many jobs have you had?",
				"answer": "<p>5 jobs - I started out doing database support and backend work, moving to front end web development, then UX and design. I’ve found myself slowly moving up the stack (over about 20 years) following my interests as they evolved!</p>\n<p><em>[Mike: Big change - what made you go from back-end to front-end?]</em></p>\n<p>A big change indeed! I think it was mainly curiosity how things worked on the front end, and then once I was doing front end dev I started reaching out to customers for their input, and doing some user testing and realised the real value people working in both design and UX provide, so wanted to learn more about that.</p>\n<p>I’m a firm believer that people can have many careers (often in 3-5 year chunks), and shouldn’t stay doing anything when they feel they have outgrown it. Trying different things broadens the mind, and makes you aware of how other disciplines work, which can be helpful as you progress in your career.</p>\n<p>I’m happiest being a generalist and found each of those roles had enough crossover in skills to bridge the gap to move onto the next one, while I learnt.</p>\n<p>I didn’t know ‘what I wanted to do when I grew up’ but knew I wanted to go to university, so did a course that was split between Business, Computing, and HCI (human-computer interaction, which evolved into what we know as UX today). It was a ‘sandwich’ course, with a year working for Microsoft in SQL Server support in my third year - this was my first exposure to working in the tech industry and was invaluable. </p>\n<p>When I started it was very much expected that people had a degree, but now this isn’t the case - some of the most brilliant people I work with have gone straight into working and gaining experience, which I think now is the most important thing one can have.</p>\n<p>While at university I got into frontend development, working part-time doing HTML/CSS editing for some of the university departments, learning about web standards and accessibility. I then ended up working at several agencies in Oxford for several years, before leading the web team for Nominet, the UK domain registry. </p>\n<p>My interest in UX &amp; design then grew, and I started UXOxford (a UX meet-up/speaker series) which I co-ran for several years. I then moved to live by the sea, while working designing and building enterprise search experiences for a search consultancy, before moving agency-side where I am now.</p>\n"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "Some of the most brilliant people I work with have gone straight into working and gaining experience,"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What is your tech stack? What are your websites running on? What do you develop on/with?",
				"answer": "<p>At work: Python/Django on the backend - we have a very strong engineering team and build a lot of APIs. On the front end it varies - usually React with accessible HTML/CSS, and we occasionally use Wagtail - a Django-based CMS.</p>\n<p>As I’m a designer who can code, I usually start with a design tool (like Sketch) and prototype quickly. While people seem to respond well to visual mockups, I like getting things into code as soon as I can, as that's where one can see the real edges of things, and you can properly understand how something works in a real browser. </p>\n<p>Even spiking out things with mock data in a ‘living’ style guide can be helpful, as one can see how things behave responsively and can be tested in different devices/browsers more easily, even before you wire things up and develop properly.</p>\n<p>For my own site I have a website built in <a href=\"https://getkirby.com\">Kirby CMS</a> which I love, and use Netlify for things that can be static.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "It’s amazing how much you can improve existing projects with small adjustments rather than throwing things out and starting again."
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about your job?",
				"answer": "<p>The variety. Whether I’m working on a single project, or across a variety of clients, there are always dozens of things to do, whether exploratory design workshops at early stages or prototyping design, to doing UX audits on an existing project - it’s amazing how much you can improve existing projects with small adjustments rather than throwing things out and starting again.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What could be better?",
				"answer": "<p>There are always things one could improve process-wise, and I’m constantly looking at my design process, and what could be done better. Most of the time people focus on tools and technology, but the biggest improvements can be made in how we communicate with one another. Tools that help us get us to working code faster are good - I’m heartened by the progress of design systems and patterns, and design tokens that help both developers and designers get to something working faster. </p>\n<p>I’m currently reading <a href=\"http://shop.oreilly.com/product/0636920033561.do\">Discussing Design by Adam Connor and Aaron Irizarry</a> and <a href=\"https://rosenfeldmedia.com/books/build-better-products/\">Building Better Products by Laura Klein and Kate Rutter</a> which are both excellent.</p>\n"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "The biggest improvements can be made in how we communicate with one another"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about the web industry?",
				"answer": "<p>Its love of sharing and community - very open to new ideas and ways of working. One can’t help being inspired by going online and seeing what people are sharing and working on. I can’t think of another industry that's so sharing.</p>\n"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What are your frustrations with the industry?",
				"answer": "<p>Two things. One the lack of diversity in both gender and race of who works in the industry - I’m happy the subject has come more to the fore recently, but know there’s loads more we need to do in terms of equality and balance. </p>\n<p>As a white male, I’m fully aware that I’ve had a career in an industry that favours people who look and sound like me, and I know that every time I get the opportunity to work with a more diverse range of people both my perspective and my work benefits massively.</p><p>As both a designer and photographer, websites like <a href=\"https://techiesproject.com/\">Techies</a> and <a href=\"https://thegreatdiscontent.com/\">The Great Discontent</a> have been massively influential on my thinking. They show the outside world a more comprehensive picture of people who work in tech, broadening who I follow and pay attention to in our industry.<br /><br />I'd encourage you to explore sites like these and start listening to, following and promoting under-represented people, whether people of colour, women, folks over 50, LGBTQIA folks, working parents, the disabled, etc. There's plenty of room for all of us, and I guarantee you'll leave inspired.</p>\n<p>Secondly, tribalism. I’ve seen people in the industry (both developers and designers) get hot under the collar over the libraries and tools they use when the reality is that different tools are each good for different uses, and we should all be more accepting of people who are at different levels of experience in their career.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "Don’t compare yourselves to others too much"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "Knowing what you know now, if you were to start again in the industry would you do anything differently?",
				"answer": "<p>I don’t think I would change anything - good or bad - everything is a learning experience. I'm lucky to work in an industry where people are willing to share knowledge, and I'm always learning from others, both as a designer and developer.</p>\n<p>If I'm feeling tired or burnt out, I like getting away from technology and screens for a bit. Going for a run or doing something creative with your hands gives the brain a rest and recharges you. You’ll find some of your best ideas will come when doing other things like this.</p>\n<p>My advice to you is to be open to trying new things and follow your heart. Try not to compare yourselves to others. Most of what people are putting online is a highlight reel, and most of us are learning as we go in reality. </p>\n<p>Do things you don’t know how to do, share and document your work - whatever stage you’re at, there’s always going to be people learning coming up behind you! But don’t try and fill every waking hour with design or development, as that way burnout beckons. You don’t have to work all the time - be kind to yourself and have some fun!</p>\n"
			}
		}
	]
}
---
