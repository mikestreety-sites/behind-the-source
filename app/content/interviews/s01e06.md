---json
{
	"date": "2020-07-13",
	"title": "S01E06",
	"permalink": "/interviews/s01e06-stuart-nelson/",
	"heading": "Some days I just feel like I’m playing a game and it almost doesn’t feel like a real job",
	"intro": "<p>Stuart approached me when I first floated the idea of Behind the Source to Codebar where he is a coach in Bristol. He had some helpful tips (and was one of the first to hear the name). This kind of interview is why I wanted to do this in the first place; you can tell Stuart is passionate about the web and loves what he does.</p>",
	"name": "Stuart Nelson",
	"photo": "/assets/img/people/s01e06.jpg",
	"ogImage": "/assets/img/social-media/s01e06.png",
	"social": [
		{
			"href": "https://northernbadger.co.uk/",
			"title": "northernbadger.co.uk",
			"type": "website"
		},
		{
			"title": "stuartjnelson",
			"type": "twitter"
		}
	],
	"body": [
		{
			"type": "question",
			"fields": {
				"question": "What is your job title (and feel free to talk about where you work)?",
				"answer": "<p>I'm a <strong>Lead Frontend engineer</strong> - I work at <a href=\"https://studee.com/\">Studee</a>, we make it simple to find, apply &amp; enrol at universities abroad. <br /><br />We currently have an engineering team of 7. Lead engineer, 3 backend engineers, QA, frontend/middleware and myself. We are a small team and everyone takes ownership of their part of the stack.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "How did you get where you are today? What is your education background? How many jobs have you had?",
				"answer": "<p>I went to uni and studied Commercial Music, which was a songwriting/performance/entrepreneurial degree. We had to make a website for a band on the course which was my first experience of web development. </p>\n<p>In my final year, I had to write a dissertation (which was daunting for me because I'm dyslexic) so my housemate and I did a swap; he helped me write my dissertation and I made him a website for one of his modules. Someone saw that site and asked me to make one for them - I said I'd do it for £50 and ever since I've been blagging it.</p>\n<p>I worked for myself creating WordPress sites for small businesses when I first started out. A couple of years ago I contracted for a few UK companies while living out on the west coast of Canada. I've had a couple of creative agency jobs and now I've got my first permanent product role. I really like my current role as I've been able to specialise just working on the frontend.</p>\n<p>I’ve always wanted to live in the mountains for a winter season. My ideal place to go was North America but Canada is a lot easier to get a visa than the USA. I was also interested in living in Canada as a place for the future. My partner and I went to Whistler which is the largest resort in Canada and had an awesome time. I managed to get 91 days in on the mountain.. We were there for 8 months then bought an old converted van and spent 3 months doing 7000 miles around the west coast of Canada and the USA. We decided to come back after our trip to the UK. </p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "The hardest part I find is keeping it as DRY as possible and naming things!"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What is your tech stack? What are your websites running on? What do you develop on/with?",
				"answer": "<p>On the frontend of the Studee website, we are using <a href=\"https://mustache.github.io/\">Mustache</a> for templating. This allows us to create the components for our Pattern library. The data files are all <em>.json</em> and the HTML is handcrafted by us. For styles we use Sass and all the JavaScript is only presentational. It's compiled with Rollup.js and then Docker is used to get everything running.</p>\n<p>I’ve been creating pattern libraries for a good five years now so I’ll be honest I don’t really think about it too much. The hardest part I find is keeping it as DRY as possible and naming things. I’m quite into Sass which I think makes life a heck of a lot easier when it comes to maintaining the pattern library.</p>\n<p>The middleware at work is a Node.js and Express setup. As much as possible our API does the bulk of the work which is written in PHP/symphony - I don’t touch the API code!</p>\n<p>My computer at work is a 13” MacBook Pro, which is pretty rad. Before I’ve only used a 15” laptop but now I think the slightly smaller size and lighter machine is the way forward. I haven’t used a PC in a while but would not be against it especially considering how expensive Macs have become.</p><p>At home, I have a few half-finished side-projects but the only open source one I’ve ever finished is <a href=\"https://ba.northernbadger.co.uk/\">Badger Accordion</a> which is an accessible lightweight, vanilla JavaScript accordion with an extensible API and it is only 8.17kb (Gzipped 2.49kb). </p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about your job?",
				"answer": "<p>I work in a really good team. Everyone is highly skilled but more importantly is nice to work with.We have four sh!t rules at Studee:<br /></p>\n<ul><li>Give a shit</li><li>Don’t talk shit</li><li>Get shit done</li><li>Don’t be a shit</li></ul>Web accessibility is a big passion of mine. Through my role I’m able to learn a lot more about accessibility and put it at the centre of my work. Through the coaching I receive at work I’ve had the confidence to create a workshop; Accessibility 101 and presented it to the whole company. Off the back of that other parts of the business really started to grasp it. Now we’re writing an accessibility statement, the content team is really behind it and I will be talking to our clients about it. I’ve been able to give the talk at a bunch of local meetups too.<br /><br />Work life balance is respected at Studee - I get to work remotely four days a week and in general we’re super flexible as a company. We aren’t measured on how much time we work but on our output. I’m a big believer this is how all business should be.<br /><p>We have business projects that we break down into engineering projects. An engineering project is then made up of a series of deliverable tasks which are no more than 3 days work. On a Thursday we go over what we have defined for the next week and decide how much work we can take on. If I didn’t take on enough work we always have non-critical defects or code improvements I can work on. Because the culture is so honest and positive at Studee, I don’t worry about my output too much. We really trust each other to get the work done and meet the business deadlines.<br /><br />Studee is <a href=\"https://studee.com/giving-back/\">passionate about taking responsibility for its impact as a business</a>. We get three paid days a year off to volunteer for organisations that we care about. We’re creating scholarships through a charity in Myanmar to give students there the opportunity to further their studies. Studee are also planting trees for every student we enrol to offset the carbon of their flights.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "Studee is passionate about taking responsibility for its impact as a business."
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "Is there anything you think that could be better?",
				"answer": "<p>I’ll be honest. Nope. I feel very lucky to be working in such a trusting company. There’s a lot of give and take. The result is I feel very motivated and relaxed. I’ll be left to do my work however I see fit and there is always someone in the team around to help out if I get stuck.</p>\n<p>It would be really nice, however, if we could hire some more Junior Engineers but right now we are a startup and need to scale <em>quickly.</em><br /></p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about the web industry?",
				"answer": "<p>Bringing together visual creativity and completed logic problems. Some days I just feel like I’m playing a game and it almost doesn’t feel like a real job. There are a lot of great opportunities out there and you don’t need a formal education to get them. I think, in general, there are a lot of really nice people in the industry willing to help each other and try to make their products available to everyone.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "There are a lot of great opportunities out there and you don’t need a formal education to get them"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What are your frustrations with the industry?",
				"answer": "<p>I feel since I started tinkering back in 2013 it's a lot more complex to get going. I started with a PHP WordPress theme, CSS and jQuery. I know there are zero config build tool options out there but I never managed to get them to just work straight away. <br /><br />JavaScript frameworks are super cool and can be the perfect tool but they shouldn’t be the thing you immediately reach for. There needs to be a reason you’d be reaching for a framework as opposed to a different solution. Working at Studee with the tech stack that was started before I came on board has really reinforced this. I hadn’t used Mustache or Node/Express before but they are very readable with a low barrier for entry. I didn’t get it right away but after a few weeks it all made sense. The reason I was able to understand Node/Express is because I’ve been lucky to have time to learn vanilla JavaScript. I’m not the world's best JS dev by any means, but I know enoughto be able to transfer my skills to any framework.<br /><br />Accessibility should be the first thing we think of. If you do then it solves a LOT of other issues. SEO, valid HTML, progressive enhancement, performance etc. The main factor for me is it’s the <em>right</em> thing to do. In 2016 I went to Render conf and Robin Christopherson, an accessibility legend, did this amazing talk about considering <a href=\"https://vimeo.com/165995026\">how making your website accessible doesn’t just benefit disabled people</a>; it benefits everyone. I HIGHLY recommend checking it out - after watching this I was inspired to try and make everything from there on out as accessible as possible.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "Accessibility should be the first thing we think of."
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "Knowing what you know now, if you were to start again in the industry would you do anything differently?",
				"answer": "<p>I wish I’d learnt JavaScript differently. I learnt in dribs and drabs and got <em>very</em> stuck at the start. It meant there are some things now I get stuck on that I feel if I’d learnt in a more structured way at the start, I’d be in a better place now.<br /><br />I would have looked for a permanent role sooner/looked to attend something like <a href=\"http://codebar\">Codebar</a> to learn with others. As soon as I got into a team at my first agency, Mr B &amp; Friends my skills exponentially accelerated. It wasn't just having senior people around me to help me when I was stuck. I found that by simply sitting with them while they worked taught me a lot. It also made me feel a lot better about being stuck because everyone was getting stuck sometimes - even the lead dev with 10 years experience.</p>"
			}
		}
	]
}
---
