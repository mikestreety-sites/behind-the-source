---json
{
	"date": "2020-05-18",
	"title": "S01E03",
	"permalink": "/interviews/s01e03-stephanie-stimac/",
	"heading": "I work on the thing that gives millions of people access to the internet and I get to empower developers to build for it",
	"intro": "<p>I came across Stephanie when I was listening to <a href=\"https://www.smashingmagazine.com/2020/01/smashing-podcast-episode-8/\">Episode 8</a> of the the Smashing Podcast. I was in the process of curating Series One and thought it would be fascinating to have a developer who works on a browser as part of the series. The <a href=\"https://webwewant.fyi/\">Web We Want</a> initiative is awesome and despite the stigma that working on Microsoft Edge sometimes has, it sounds like, with the move the Chromium, Microsoft are really pushing the web and its standards forward.</p>",
	"name": "Stephanie Stimac",
	"photo": "/assets/img/people/s01e03.jpg",
	"ogImage": "/assets/img/social-media/s01e03.png",
	"social": [
		{
			"href": "https://stephaniestimac.com",
			"title": "stephaniestimac.com",
			"type": "website"
		},
		{
			"title": "seaotta",
			"type": "twitter"
		}
	],
	"body": [
		{
			"type": "question",
			"fields": {
				"question": "What is your job title?",
				"answer": "<p>My title is <strong>Program Manager</strong> for Microsoft Edge Developer Experiences, but if you ever see me speak at a conference or meetup, I’m fond of calling that a “fancy” title I acquired for myself. </p>\n<p>I’ve taken to calling myself a <strong>Design Technologist</strong> because I live at this intersection of design and front-end development and sort of gravitate back and forth between those two worlds. </p>\n<p>My day-to-day role depends on whether or not I’m travelling for a conference and it is a bit different now compared to when I was doing design. Some days are packed full of meetings with maybe 1-3 hours free for actual work. </p>\n<p><em>Actual work</em> for me right now is a little bit bonkers, I’m usually: reviewing submissions for <a href=\"https://webwewant.fyi/\">The Web We Want</a> initiative, doing research and compiling information in partnership with other team members on Edge for conference/meetup talks, working with partners outside of Microsoft, checking Twitter and responding to feedback from developers, helping to manage the Microsoft Edge developer portal and documentation pages, and, when I have a spare moment, I’ll pop over onto the <a href=\"https://webhint.io/\">webhint.io</a> repo to fix some design backlog issues. <em>[Mike: Blimey, that is busy!]</em></p>\n<p>I have so many things that I own at the moment, there is no real “typical” day for me.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "How did you get where you are today? What is your educational background? How many jobs have you had?",
				"answer": "<p>I graduated in 2010 with a Bachelor's Degree in New Media Design, basically, digital design. I’m not super happy with the experience I had at university when it came to my area of study and feel like I missed out on some fundamentals when it came to design, but I graduated and getting that first job was really hard. </p><p>I had 3 jobs before Microsoft, not including the freelance work I used to do on the side. I worked part-time for a small local company in Bellingham, WA doing very unglamorous design work in an unglamorous office (it was a barn) and was just hungry to get experience. So I spent 6 months there and was then hired on at a start-up in Bellevue, WA called Point Inside that had a mobile app. I spent a year doing production design work and user interface work for the app. </p><p>After a year I was referred by a former classmate for a position at a Public Relations agency on their creative team. That was my so-called “big” break. I spent 4 years at the agency and worked my way up from doing production work, like designing PowerPoint templates, event invites, miscellaneous print materials and infographics to becoming an Experience Designer. I was responsible for the entire project lifecycle when it came to websites: I’d do the research, wireframes, visual design and front-end development. We’d hire a developer when we needed some more complex functionality that was out of my skill range. </p><p>That was where I really fell in love with the web and building for it. After 4 years at that agency, I was hired onto the Microsoft Edge team as a Program Manager but I was doing design work for our developer-facing web properties and only in the last year have I switched out of that design role into a more developer engagement and advocate type role. </p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What is your tech stack? What languages do you use? What are your projects built with? Do you interact with servers, if so, what kind? What do you develop on/with?",
				"answer": "<p>HTML and CSS are at the core of what I do when I’m working on a project. I understand JavaScript and its capabilities but I’m not very good at writing it -- I’ve never had to be. I’ve usually worked with a team where I could hand off my HTML and CSS and they would make whatever changes they needed to and add in JavaScript. </p><p>Lately, I’ve been heavy into using <a href=\"https://www.11ty.dev/\">11ty</a> for projects so that’s been fun to explore and learn how to work with JSON files and data. </p><p>My primary machine at work is a MacBook <em>[Mike: A Microsoft Edge employee on a Mac! Whatever next]</em> but I switch between Windows and Mac frequently and I use VS Code. At home, I have an iMac because I’m still a designer at my core and do a bit of photography on the side in addition to coding. </p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "I constantly find myself in awe of the role I’m in"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about your job?",
				"answer": "<p>I constantly find myself in awe of the role I’m in. I don’t just get to work on the web building websites, I work on the thing that gives millions of people access to the internet and I get to empower developers to build for it. A lot of my role lately has been focused on an initiative called <a href=\"https://webwewant.fyi\">The Web We Want</a> and identifying pain points that developers have when building for the web. </p><p>I actually really enjoy engaging with developers and digging into their problems to figure out how we can solve them or make their experience in a product better.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What could be better?",
				"answer": "<p>As it goes with very large companies, there can be a lot of bureaucracy and projects can take a long time to get started. Sometimes that’s just the culture and it can take a very long time to change if you can change it at all.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "You can never stop learning if you work on the web"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about the web industry?",
				"answer": "<p>There are so many amazing people I’ve met at conferences with such a wealth of knowledge. Getting to engage with them and learn from them is one of my favourite things. I love how big the industry is too, as you grow and gain experience, everyone kind of has their own little niche whether that’s performance or security or CSS, there are so many opportunities to learn. You can never stop learning if you work on the web because of the pace of things and I love that.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What are your frustrations with the industry?",
				"answer": "<p>We unfortunately still have a long way to go when it comes to being inclusive and being aware of our unconscious biases. There are great people doing great work, like <a href=\"https://tatianamac.com/\">Tatiana Mac</a>, and I always appreciate her perspective and really try to take to heart what she shares.</p><p>I was looking over a conference lineup the other day and it was all white men, and as a woman in the industry, it frustrates me that this is still an issue. There was a CFP open for the conference still and I immediately decided not to submit anything for it. If you’re a conference organiser, I believe you should have a diverse lineup of speakers, otherwise, how do you help build an inclusive industry? </p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "If you’re a conference organiser, I believe you should have a diverse lineup of speakers, otherwise how do you help build an inclusive industry?"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "Knowing what you know now, if you were to start again in the industry would you do anything differently?",
				"answer": "<p>Honestly, I wouldn’t. </p>\n<p>The path I took led me to a job that I love and I wouldn’t do anything differently. For me, I’m lucky that I get to learn as a part of my job. I get to keep my finger on what’s hot in the dev community, what tools are people using, what framework, etc. And I believe you should never stop learning because there’s always something new <em>to </em>learn. If you went to university, you don’t graduate knowing it all, and perhaps that’s one thing I wish I could tell myself: it’s okay to not know it all, you don’t know and you never will, but curiosity and willingness to learn new things will take you far.</p>"
			}
		}
	]
}
---
