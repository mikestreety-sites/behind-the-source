---json
{
	"date": "2020-05-04",
	"title": "S01E01",
	"permalink": "/interviews/s01e01-sara-vieira/",
	"heading": "Sometimes we lose track that the web was built for people and not for just us",
	"intro": "<p>Sara and I have followed each other on twitter for a long time. When I started publishing tutorial YouTube videos, she was one of the first (and only) to encourage me. This small act of kindness means that I have a huge amount of respect for her and have learnt that she is pretty dope. When this project progressed from an idea to realisation, she was top of my list to ask to be involved and I was ecstatic when she said yes.</p><p>You may recognise her from speaking at many different conferences, <a href=\"https://leanpub.com/opinionated-guide-to-react\">from her book</a>, or just from being kick-ass on Twitter. Or you might not - that doesn't matter.</p>",
	"name": "Sara Vieira",
	"photo": "/assets/img/people/s01e01.jpg",
	"ogImage": "/assets/img/social-media/s01e01.png",
	"social": [
		{
			"title": "NikkitaFTW",
			"type": "twitter"
		},
		{
			"title": "SaraVieira",
			"type": "github"
		}
	],
	"body": [
		{
			"type": "question",
			"fields": {
				"question": "What is your job title?",
				"answer": "<p>My job title is \"<strong>Software Engineer</strong>\", which means I mostly do front-end (mainly <a href=\"https://reactjs.org/\">React</a>) with some Node sprinkled here and there. I work for <a href=\"https://codesandbox.io/\">CodeSandbox</a>, which is an online code editor that - we aim to hopefully allow more people to learn how to do front-end while empowering the rest of us to share our code more easily.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "How did you get where you are today? What is your educational background? How many jobs have you had?",
				"answer": "<p>I was born in fuck-town nowhere in Portugal, I think we have a population of about 5,000 people and it is so small, the hospital I was born in even closed.</p><p>I got to be a developer (and a well-paid one) with a bit of luck and a lot of work (but in truth, I think that’s what we all say even if luck isn’t an actual thing we can touch and quantify). <br /></p><p><em>Looks at LinkedIn profile...</em></p><p>I have been working in this industry since 2014; over that time I have had 6 jobs - most of them were pretty good placements where I was able to grow, learn and discover new things that kept me excited about tech. That isn't to say they were all good, but I am proud of my country for the decent jobs we have in terms of environment, not money (we don’t have that) but environment.</p><p>I was actually in my first job while I was an intern when I got into speaking, before getting a job to pay for my stuff (like gas) I did a bunch of articles that paid me and got me contacted by Front Trends (a conference in Warsaw) - I swear at first it was a scam and I was going to get kidnapped &#x1f602; <em>[Mike: Thank god she didn't, although I'm sure we could have made a Kickstarter to pay the ransom...]</em></p><p>After that first talk, I continue to share my knowledge which helps with the anxiety issues I had, and still have. The anxiety does get better but no matter how many times you speak you are still terrified of it.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "The anxiety does get better but no matter how many times you speak you are still terrified of it"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What is your tech stack? What are your websites running on? What do you develop on/with?",
				"answer": "<p>I mostly work with React daily, the CodeSandbox website is built with React and so are about 70% of my side projects. As for the rest, I use a lot of <a href=\"https://styled-components.com/\">styled-components</a> for styling and the rest varies a lot by project.</p><p>I do like React <em>a lot.</em> I also love Vue, but always end up using more React as I am faster in building things, since I have been using for about 4 years. I like to build things fast, and that’s the reason I don’t have a lot more variety in frameworks in my side projects.</p><p>As for hosting these side projects, I would say 90% are hosted in <a href=\"https://www.netlify.com/\">Netlify</a>, 9% on <a href=\"https://zeit.co/\">Zeit</a> (mostly the Node ones, like small serverless functions) and one is on <a href=\"https://www.digitalocean.com/\">Digital Ocean</a>. At least all my domains are on <a href=\"https://www.namecheap.com/\">Namecheap</a> so I can have one constant in my dev environment.</p><p>As for my “machine”, I have a 13” inch MacBook without a touch bar and I love him.</p>"
			}
		},
		{
			"type": "image",
			"fields": {
				"image": [
					"732"
				]
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about your job?",
				"answer": "<p>The people I work with are some of the kindest humans I have ever met. They all just want to see us be better and happier people. I love my team in all ways, they even made a video when I got married last year.</p>\n<p>We are a team of 9. Some of us work in the HQ in Amsterdam while the rest work remotely from several parts of Europe. I love the product I work on, I think it helps developers starting out and makes sharing of code much easier.</p>\n<p>It’s such a nice feeling to be able to build a platform to code, with code, I feel like I have to get back to the basics, in trying to work out I would have liked to have as I was starting out.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What could be better?",
				"answer": "<p>Honestly, I don’t know, the only thing I can think of is that it would be awesome is if the EU would get their shit together so I didn’t have to be a <em>freelancer</em> to work for a company in the <em>Netherlands</em> living in Germany, but that’s not on the job.</p><p>I mean I also didn’t <em>really</em> approve our transition to <a href=\"https://www.typescriptlang.org/\">TypeScript</a> - but, well, nothing can be perfect.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "It would be awesome if the EU would get their shit together"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What do you love about the web industry?",
				"answer": "<p>Our willingness to share and learn from each other is amazing. We are more than willing to give back and try to teach the next person - either by mentoring or open-source. We are generally a good bunch of people that just want more people to join the industry.</p>"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "What are your frustrations with the industry?",
				"answer": "<p>The fact that there seems to always be a fight to \"who is the smartest person that is going to build the greatest thing\" and sometimes we lose track that the web was built for people, and not for just us.</p><p>This is something that I think can be seen as good in one sense, as it is more competition in the open-source world, but, that’s the thing, open source shouldn’t be a competition. It should be about all of us joining together and trying to make the best tool ever, instead of always trying to make the most awesome JavaScript library anyone has ever seen.</p>"
			}
		},
		{
			"type": "pullquote",
			"fields": {
				"quote": "Sometimes we lose track that the web was built for people and not for just us"
			}
		},
		{
			"type": "question",
			"fields": {
				"question": "Knowing what you know now, if you were to start again in the industry would you do anything differently?",
				"answer": "<p>I would not have been a developer advocate.</p><p>A developer advocate is someone who gets paid to travel the world and teach people things, or that’s what they sell you on. In the end, you (or at least me) ,end up being a fancy PR person that can make buttons. It also requires so <em>muuuuch</em> traveling, which was spiralling me into a mental breakdown. </p><p>Most of my joy in this job comes from coding and building things. I like teaching people but after being a developer advocate and a developer, I learned that I can get the best of being a developer advocate and do it in a developer position, all it takes is finding a good company.</p>"
			}
		}
	]
}
---
