---
layout: page.njk
title: The Podcast Process
seoPageTitle: The process of featuring on the podcast
seoPageDescription: What are the steps for recording a podcast? What is needed and what is provided
permalink: /podcasts/process/
---

The podcast is fairly new, so this process might change as the podcast progresses.

Behind the Source podcast is aimed at people who haven't heard of, or worked with, the subject matter before. Think of it like a chat with a junior or your friend who is getting into web development. We don't get too technical as it is about introducing the topic - see below for some example questions.

## Planning

1. Once we've established the subject you'll be talking about, I'll ask for your email address
2. I will create a Google Doc with some example questions for you to think and plan your answers - I'll try not to surprise you with any curve-balls. You are more than welcome (and encouraged) to make your own notes in the Google Doc as that may spur some more questions. Some example questions are:
   1. What is XXX?
   2. When do you use XXX?
   3. When don't you use XXX?
   4. What are the alternatives?
3. When you're happy with the questions, we will plan a time and date to record - it shouldn't take more than an hour or so

## Recording

1. I'll send a link to Zencastr  - this is browser based and needs nothing downloading. It records your audio track locally
2. Once you are comfortable, I'll click record and start the podcast (I would like to record video & audio)
3. There will be a chance for you to introduce yourself and plug/promote anything you have to plug/promote
4. Before we start talking about your subject, I'll ask: What are you excited about learning? What are you a "junior in" that you want to expand your knowledge on
5. We'll then talk about your chosen subject
6. At the end I'll ask if you have any questions for me and ask for your social & website links
7. When the recording stops, I'll ask you to keep the tab open while the audio uploads
8. That is it!

## Release

1. At time of recording, I should have an idea of when the podcast will be released
2. Before releasing I'll send you a link so, if you wish, you can listen and feedback
3. On release day I will tweet a couple of times and send an email out to the mailing list

---

Any questions, just ask!

Now go and [listen to the podcasts](/podcasts/) or [read some interviews](/interviews/).
